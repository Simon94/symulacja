#pragma once
#include <Windows.h>
#include <iostream>
#include <ctime>
#include "Stado.h"
#include"Sterowanie.h"
using namespace std;
class Plansza
{
public:
	HANDLE hOut;
	Ludzik* plansza[30][100]; 
	bool jedzenie[30][100];
	int liczba_ludzikow = 6;
	Stado K;
	time_t czas;
	time_t czas_startowy;
	int s = 0;
	int m=0;
	int licznik = 0;
	int liczba_samic = 0;
	int liczba_samcow = 0;
	int kroliki_na_planszy=0;
	int kroliki_od_poczatku = 0;
	int ostatnie_id = -1;
	Sterowanie odczyt;

	void PomiarCzasu();
	void WypelnianiePlanszy(Ludzik* tmp);
	void UstawieniaPoczatkowe();
	void Rysowanie();
	void Zmiana(int id,int x, int y);
	bool ObliczaniePredkosci(int licznik);
	void ZmianaWektora(int id);
	void UzupelnianieInformacji();
	void DodajKrolika(int id);
	void SzukaniePartnera(int i, int j);
	void Jedzenie();
	Plansza();
	~Plansza();
};

