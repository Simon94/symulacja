#include "Stado.h"


void Stado::DodajKrolika(int id)
{
	K.DodajNaPoczatek(Head, Tail, id);
	if (id<2)
		Head->plec = 'F';
	else if (id < 4 && id >= 2)
		Head->plec = 'M';
	else 
		LosojPlec();
	Head->czy_zyje = true;
	Head->zycie = 300;
		
}
void Stado::LosojPlec()
{
	srand(time(NULL));
	int tmp = rand() % 2;
	if (tmp == 0)
		Head->plec = 'F';
	else
		Head->plec = 'M';
}
void Stado::ZmianaPredkosci()
{
	srand(time(NULL));
	Current = Head;
	while (Current != nullptr)
	{
		if (Current->czy_zyje == true)
		Current->predkosc = rand() % 11;
		Current = Current->next;
	}
}
void Stado::ZmienWektor()
{
	srand(time(NULL));
	Current = Head;
	while (Current != nullptr)
	{
		if (Current->czy_zyje == true)
		{
			Current->vx = rand() % 3 - 1;
			Current->vy = rand() % 3 - 1;
			//zmiana bezplodnosci
			if (Current->plec == 'F' && Current->okres_bezplodnosci > 0)
				Current->okres_bezplodnosci--;
		}
		Current = Current->next;
	}
}
void Stado::ZmianaWieku()
{
	Current = Head;
	while (Current != nullptr)
	{
		Current->wiek++;
		if (Current->wiek>10)
		if (Current->plec == 'M')
			Current->plodny = true;
		else if (Current->plec == 'F' && Current->okres_bezplodnosci <= 0)
			Current->plodny = true;

		Current = Current->next;
	}

}
void Stado::ZnajdzKrolika(int id)
{
	K.ZnajdzKrolika(Head, Current, id);
}
void Stado::Ciaza(int id)
{
	ZnajdzKrolika(id);

	Current->plodny = false;

	//Current->ciaza = true;
	//Current->wiek_potomka = 0;
	Current->okres_bezplodnosci = 10;
}
void Stado::Obserwacja()
{
	Current = Head;
	while (Current != nullptr)
	{
		if (Current->wiek>300)
		{
			Current->czy_zyje = false;
		}
		if (Current->zycie <= 0)
		{
			Current->czy_zyje = false;
		}
		Current = Current->next;
	}
}
Stado::Stado()
{
}


Stado::~Stado()
{
}
