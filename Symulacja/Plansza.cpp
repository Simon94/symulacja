﻿#include "Plansza.h"

void Plansza::WypelnianiePlanszy(Ludzik* tmp)
{
	COORD c;
	int j=0, i=0;

	c.X = 0;
	c.Y = 0;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << (char)0xC9;

	for (j = 1; j <119 ; j++)
	{
		c.X = j;
		c.Y = i;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << (char)0xCD;
	}

	c.X = 119;
	c.Y = 0;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << (char)0xBB;
	
	for (i = 1; i <30; i++)
	{
		c.X = j;
		c.Y = i;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << (char)0xBA;
	}
	c.X = 119;
	c.Y = 30;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << (char)0xBC;
	
	for (j = 118; j >0; j--)
	{
		c.X = j;
		c.Y = i;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << (char)0xCD;
	}
	c.X = 0;
	c.Y = 30;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << (char)0xC8;
	for (i = 29; i >0; i--)
	{
		c.X = j;
		c.Y = i;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << (char)0xBA;
	}
	c.X = 100;
	c.Y = 0;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << (char)0xCB;

	for (i = 1; i <30; i++)
	{
		c.X = 100;
		c.Y = i;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		cout << (char)0xBA;
	}
	c.X = 100;
	c.Y = 30;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << (char)0xCA;

	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	c.X = 101;
	c.Y = 1;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "Liczba krolikow" ;
	c.X = 101;
	c.Y = 2;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout<<"od poczatku : ";

	c.X = 101;
	c.Y = 5;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "Liczba krolikow";
	c.X = 101;
	c.Y = 6;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "na planszy : ";

	c.X = 101;
	c.Y = 9;
	SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_INTENSITY);
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "Liczba samic :";
	c.X = 101;
	c.Y = 11;
	SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "Liczba samcow :";

	c.X = 101;
	c.Y = 28;
	SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY);
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "Czas symulacji :";

	for (int i = 0; i < 30; i++)
	{
		for (int j = 0; j < 100; j++)
		{
			plansza[i][j] = nullptr;
		}
	}
	srand(time(NULL));
	
	while (tmp!=nullptr)
	{
		kroliki_od_poczatku++;
		int x = rand() % 99;
		int y = rand() % 29;
		plansza[y][x] = tmp;
		tmp=tmp->next;
	} 
}
void Plansza::UstawieniaPoczatkowe()
{
	//ukrycie kursora
	::HANDLE hConsoleOut = ::GetStdHandle(STD_OUTPUT_HANDLE);
	::CONSOLE_CURSOR_INFO hCCI;
	::GetConsoleCursorInfo(hConsoleOut, &hCCI);
	hCCI.bVisible = FALSE;
	::SetConsoleCursorInfo(hConsoleOut, &hCCI);

	//zmiana koloru
	system("COLOR F0");

	//zmiana rozmiaru okna
	_COORD coord;
	coord.X = 120;
	coord.Y = 32;

	_SMALL_RECT Rect;
	Rect.Top = 0;
	Rect.Left = 0;
	Rect.Bottom = 30 - 1;
	Rect.Right = 100 - 1;

	HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE);      // Get Handle 
	SetConsoleScreenBufferSize(Handle, coord);            // Set Buffer Size 
	SetConsoleWindowInfo(Handle, TRUE, &Rect);            // Set Window Size
	for (int i = 0; i < 30; i++)
	{
		for (int j = 0; j < 100; j++)
		{
			jedzenie[i][j] = false;
		}
	}
	srand(time(NULL));
	for (int i = 0; i < 30; i++)
	{
		int y = rand() % 29;
		int x = rand() % 99;
		jedzenie[y][x] = true;
	}
}
void Plansza::Rysowanie()
{
	COORD c;

	hOut = GetStdHandle(STD_OUTPUT_HANDLE);

	for (int i = 0; i < 30; i++)
	{
		for (int j = 0; j < 100; j++)
		{
			
			if(plansza[i][j] !=nullptr)
			{

				c.X = j+1;
				c.Y = i+1;
				SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
				
				 if (plansza[i][j]->plec == 'M')
				{
					liczba_samcow++;
					SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
				}
				else if (plansza[i][j]->plec == 'F')
				{
					liczba_samic++;
					SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_INTENSITY);
				}
				 if (plansza[i][j]->wiek<10)
					cout << (char)0x8B;
				 else
					 cout << (char)0x8A;
					kroliki_na_planszy++;
			}
			
		}
	}
	
}
void Plansza::Zmiana(int id,int x, int y)
{
	COORD c;
	
	for (int i = 0; i < 30; i++)
	{
		for (int j = 0; j < 100; j++)
		{
			if (plansza[i][j] != nullptr && plansza[i][j]->id == id && plansza[(i + y + 29) % 29][(j + x + 99) % 99]==nullptr)
			{
				
				
				c.X = j+1;
				c.Y = i+1; 
				SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
				cout << (char)0;
				if (plansza[i][j]->czy_zyje == true)
				{
					
					if (x == 0 && y == 0)
						continue;
					else
					{
						
						plansza[(i + y + 29) % 29][(j + x + 99) % 99] = plansza[i][j];
						if (jedzenie[(i + y + 29) % 29][(j + x + 99) % 99] == true)
						{
							plansza[(i + y + 29) % 29][(j + x + 99) % 99]->zycie = 255;
							jedzenie[(i + y + 29) % 29][(j + x + 99) % 99] = false;
						}
						plansza[i][j]->zycie--;
						plansza[i][j] = nullptr;
					}
						
					SzukaniePartnera((i + y + 29) % 29, (j + x + 99) % 99);
				}
				else 
				{
					plansza[i][j] = nullptr;
				}
				i = 30; j = 100;
			}
		}
	}

}
void Plansza::PomiarCzasu()
{
	

	hOut = GetStdHandle(STD_OUTPUT_HANDLE); 

	int tmp;
	tmp = czas - czas_startowy;
	s = (tmp) % 60;
	m = tmp / 60;
	COORD c;
	c.X = 101;
	c.Y = 29;
	SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY );
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	if (m < 10)
		cout << "0" << m;
	else
		cout << m ;
	if (s < 10)
		cout << ":" << "0" << s;
	else
		cout << ":" << s;
		
}
bool Plansza::ObliczaniePredkosci(int licznik)
{
	if (K.Current->predkosc == 1 && licznik == 1)
		return true;
	if (K.Current->predkosc == 2)
		if (licznik == 1 || licznik == 6)
			return true;
	if (K.Current->predkosc == 3)
		if (licznik == 1 || licznik == 4 || licznik == 7)
			return true;
	if (K.Current->predkosc == 4)
		if (licznik == 1 || licznik == 4 || licznik == 6 || licznik == 9)
			return true;
	if (K.Current->predkosc == 5)
		if (licznik == 1 || licznik == 3 || licznik == 5 || licznik == 7 || licznik == 9)
			return true;
	if (K.Current->predkosc == 6)
		if (licznik == 1 || licznik == 3 || licznik == 5 || licznik == 6 || licznik == 8 || licznik == 10)
			return true;
	if (K.Current->predkosc == 7)
		if (licznik == 1 || licznik == 2 || licznik == 3 || licznik == 5 || licznik == 6 || licznik == 7 || licznik == 9)
			return true;
	if (K.Current->predkosc == 8)
		if (licznik == 1 || licznik == 2 || licznik == 3 || licznik == 5 || licznik == 6 || licznik == 7 || licznik == 9 || licznik == 10)
			return true;
	if (K.Current->predkosc == 9)
		if (licznik !=5)
			return true;
	if (K.Current->predkosc == 10)
		return true;
	return false;
}
void Plansza::UzupelnianieInformacji()
{

	hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD c;
	c.X = 0;
	c.Y = 0;
	//liczba od poczatku
	c.X = 101;
	c.Y = 3;
	SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_INTENSITY);
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "   ";
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << kroliki_od_poczatku;
	//liczba na planszy
	c.X = 101;
	c.Y = 7;
	SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_INTENSITY);
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "   ";
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << kroliki_na_planszy;
	//liczba samic
	c.X = 101;
	c.Y = 10;
	SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_INTENSITY);
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "   ";
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << liczba_samic;
	//liczba samców
	c.X = 101;
	c.Y = 12;
	SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << "   ";
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
	cout << liczba_samcow;
}
void Plansza::DodajKrolika(int id)
{
	K.DodajKrolika(id);
	kroliki_od_poczatku++;
}
void Plansza::SzukaniePartnera(int y, int x)
{
	for (int i = -1; i < 2; i++)
	{
		for (int j = -1; j < 2; j++)
		{
			if (i == 0 && j == 0)
				continue;
			else if (plansza[(i + y + 29) % 29][(j + x + 99) % 99] != nullptr)
			if ((plansza[y][x]->plec =='F' && plansza[(i + y + 29) % 29][(j + x + 99) % 99]->plec == 'M') && (plansza[y][x]->plodny==true && plansza[(i + y + 29) % 29][(j + x + 99) % 99]->plodny==true))
			{
				plansza[y][x]->plodny = false;

				plansza[y][x]->okres_bezplodnosci = 10;
				for (int i = -1; i < 2; i++)
				{
					for (int j = -1; j < 2; j++)
					{
						if (plansza[(i + y + 29) % 29][(j + x + 99) % 99] == nullptr)
						{
							DodajKrolika(ostatnie_id + 1);
							plansza[(i + y + 29) % 29][(j + x + 99) % 99] = K.Head;
							K.Head->zycie = plansza[y][x]->zycie / 2;
							plansza[y][x]->zycie = plansza[y][x]->zycie / 2;
							i = 2; j = 2;
							

							ostatnie_id++;
						}
					}
				}
			}
		}
	}
}
void Plansza::Jedzenie()
{
	COORD c;
	c.X = 0;
	c.Y = 0;
	srand(time(NULL));
	if (licznik==5)
	{
		int y = rand() % 29;
		int x = rand() % 99;
		jedzenie[y][x] = true;
	}
	for (int i = 0; i < 29; i++)
	{
		for (int j = 0; j < 100; j++)
		{
			if (jedzenie[i][j] == true)
			{
				c.X = j+1;
				c.Y = i+1;
				SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
				SetConsoleTextAttribute(hOut, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_GREEN );
				cout << (char)0xB0;
			}
		}
	}
}

Plansza::Plansza()
{
	srand(time(NULL));
	Jedzenie();
	UstawieniaPoczatkowe();
	for (int i = 0; i < liczba_ludzikow; i++)
	{
		K.DodajKrolika(i);
		ostatnie_id++;
	}
	WypelnianiePlanszy(K.Head);
	K.ZmianaPredkosci();
	Rysowanie();
	Sleep(1000);
	time(&czas_startowy);

	//Poczatek symulacji
	
	while (true)
	{
		Jedzenie();
		K.Obserwacja();
		if (licznik == 10)
		{
			K.ZmianaPredkosci();
			K.ZmienWektor();
			K.ZmianaWieku();
			licznik = 0;
			
			if (K.Head == nullptr)
			{
				odczyt.koniec = false;

				break;
			}
		}

		time(&czas);
		K.Current = K.Head;
		for (int i = 0; i < kroliki_na_planszy; i++)
		{
			K.ZnajdzKrolika(K.Current->id);
			if (ObliczaniePredkosci(licznik)==true)
			Zmiana(i, K.Current->vx, K.Current->vy);
			K.Current = K.Current->next;
		}
		
		kroliki_na_planszy = 0;
		licznik++;
		PomiarCzasu();
		Rysowanie();
		UzupelnianieInformacji();
		liczba_samcow = 0;
		liczba_samic = 0;
		
		Sleep(80);
	}
	
}


Plansza::~Plansza()
{
}
