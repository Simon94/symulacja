#pragma once
#include <ctime>
#include "Ludzik.h"
class Stado
{
public:
	Ludzik * Head ;
	Ludzik * Tail ;
	Ludzik * Current;
	Ludzik K;

	void DodajKrolika(int id);
	void ZmianaPredkosci();
	void ZmienWektor();
	void ZmianaWieku();
	void ZnajdzKrolika(int id);
	void LosojPlec();

	void Ciaza(int id);
	void Obserwacja();
	Stado();
	~Stado();
};

